package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Fecha: 13/10/2018
 * CopiRigth: Kevin Lovos
 * Nombre de la clase: DaoUsuarios
 * Version: 1
 * @author Kevin Lovos
 */
public class DaoUsuarios extends Conexion
{
    public List all() throws Exception
    {
        ResultSet res;
        List lsUsuarios = new ArrayList();
        try 
        {
            this.conectar();
            String sql="SELECT * FROM Usuarios WHERE estado = 1 ORDER BY id DESC";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                Usuarios us = new Usuarios();
                TipoUsuario tu = new TipoUsuario();
                us.setId(res.getInt(1));
                tu.setId(res.getInt(2));
                us.setNombre(res.getString(3));
                us.setPass(res.getString(4));
                us.setEstado(res.getInt(5));
                us.setTu(tu);
                lsUsuarios.add(us);
            }
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return lsUsuarios;
    }
    public void add(Usuarios us) throws Exception
    {
        try
        {
            this.conectar();
            String sql="INSERT INTO Usuarios VALUES (NULL, ?, ?, ?, 1)";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setInt(1, us.getId());
            pre.setInt(2,us.getTu().getId());
            pre.setString(3, us.getNombre());
            pre.setString(4, us.getPass());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    public void edit(Usuarios us) throws Exception
    {
        try 
        {
           this.conectar();
           String sql="UPDATE Usuarios SET idTipoUsuario = ?, nombre = ?, pass = ? WHERE id = ?";
           PreparedStatement pre= this.getConn().prepareStatement(sql);
           pre.setInt(1, us.getTu().getId());
           pre.setString(2, us.getNombre());
           pre.setString(3, us.getPass());
           pre.setInt(4, us.getId());
           pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    public void delete(Usuarios us) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="UPDATE Usuarios SET estado = 0 WHERE id = ?";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setInt(1, us.getId());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public Boolean auth(String user) throws Exception
    {
        Boolean estado = false;
        ResultSet res = null;
        try
        {
            conectar();
            String sql = "{CALL auth(?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, user);
            res = pre.executeQuery();
            if (res.next())
            {                
                estado = true;
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return estado;
    }
    
    public Integer verify(String user, String pass) throws Exception
    {
        Integer id = -1;
        ResultSet res = null;
        try
        {
            String sql = "{CALL verify(?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, user.replace("'", "''").replace("\"", "''"));
            pre.setString(2, pass.replace("'", "''").replace("\"", "''"));
            res = pre.executeQuery();
            if (res.next())
            {
                id = res.getInt(1);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return id;
    }
}
