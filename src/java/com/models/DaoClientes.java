
package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase: DaoClientes
 * Fecha: 12/10/2018
 * Version: 1.0
 * Copyright: Alvaro Perez
 * Autor: Alvaro Perez
 */
public class DaoClientes extends Conexion
{
    
    public List all() throws Exception
    {
        List<Clientes> lstClientes = new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql = "SELECT * FROM Clientes WHERE estado = 1 ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res = pst.executeQuery();
            while(res.next())
            {
                Clientes cliente = new Clientes();
                cliente.setId(res.getInt("id"));
                cliente.setNombres(res.getString("nombres"));
                cliente.setApellidos(res.getString("apellidos"));
                cliente.setEdad(res.getInt("edad"));
                cliente.setGenero(res.getString("genero"));
                cliente.setDui(res.getString("dui"));
                cliente.setDireccion(res.getString("direccion"));
                cliente.setTelefono(res.getString("telefono"));
                cliente.setEstado(res.getInt("estado"));
                lstClientes.add(cliente);
            }
        }
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
        return lstClientes;
    }
    
    public void add(Clientes c) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO Clientes VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, 1);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, c.getNombres());
            pst.setString(2, c.getApellidos());
            pst.setInt(3, c.getEdad());
            pst.setString(4, c.getGenero());
            pst.setString(5, c.getDui());
            pst.setString(6, c.getDireccion());
            pst.setString(7, c.getTelefono());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void edit(Clientes cliente) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Clientes SET nombres = ?, apellidos = ?, edad = ?, "
                    + "genero = ?, dui = ?, direccion = ?,"
                    +"telefono = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, cliente.getNombres());
            pst.setString(2, cliente.getApellidos());
            pst.setInt(3, cliente.getEdad());
            pst.setString(4, cliente.getGenero());
            pst.setString(5, cliente.getDui());
            pst.setString(6, cliente.getDireccion());
            pst.setString(7, cliente.getTelefono());
            pst.setInt(8, cliente.getId());
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void delete(Clientes cliente) throws Exception
    {
        try 
        {
            this.conectar();
            String sql ="UPDATE Clientes SET estado = 0 WHERE id = ?";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, cliente.getId());
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
