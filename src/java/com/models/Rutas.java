
package com.models;

/**
 * Nombre de la clase: Rutas
 * Fecha: 12/10/2018
 * Version: 1.0
 * Copyright: Alvaro Perez
 * Autor: Alvaro Perez
 */
public class Rutas {
    private int id;
    private double kilometraje;
    private String origen;
    private String destino;
    private String descripcion;
    private int estado;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Rutas() {
    }

    public Rutas(int id, double kilometraje, String origen, String destino, int estado) {
        this.id = id;
        this.kilometraje = kilometraje;
        this.origen = origen;
        this.destino = destino;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(double kilometraje) {
        this.kilometraje = kilometraje;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    
}
