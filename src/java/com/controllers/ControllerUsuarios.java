package com.controllers;

import com.google.gson.Gson;
import com.models.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Nombre del servlet: ControllerUsuarios
 * Fecha: 02/11/2018
 * CopyRigth: ITCA-FEPADE
 * Version: 1.0
 * @author Juan Pablo Elias Hernandez
 */
public class ControllerUsuarios extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoUsuarios daoU = new DaoUsuarios();
        Usuarios usu = new Usuarios();
        Gson jsonBuilder = new Gson();
        String action = (String) request.getParameter("action");
        try
        {
            if("add".equals(action))
            {
                daoU.add(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("edit".equals(action))
            {
                daoU.edit(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("delete".equals(action))
            {
                Usuarios u = new Usuarios();
                u.setId(Integer.parseInt(request.getParameter("id")));
                daoU.delete(u);
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("all".equals(action))
            {
               JSONObject item = new JSONObject();
               item.put("error", false);
               item.put("all", jsonBuilder.toJson(daoU.all()));
               out.print(item.toString());
            }
            else if("auth".equals(action))
            {
                JSONObject item = new JSONObject();
                if (daoU.auth(setData(request).getNombre()))
                {
                    Integer idTu = daoU.verify(setData(request).getNombre(), setData(request).getPass());
                    if (idTu > 0)
                    {
                        // Mostrar
                        item.put("error", false);
                        HttpSession ss = request.getSession();
                        ss.setAttribute("tu", idTu);
                        ss.setAttribute("user", setData(request).getNombre());
                        item.put("tu", idTu);
                        item.put("user", setData(request).getNombre());
                    }
                    else
                    {
                        item.put("error", true);
                        item.put("info", "Contraseña incorrecta, verifique su contraseña.");
                    }
                }
                else
                {
                    item.put("error", true);
                    item.put("info", "Usuario no encontrado, intentelo de nuevo.");
                }
                out.print(item.toString());
            }
            else if ("close".equals(action))
            {
                HttpSession ss = request.getSession();
                ss.setAttribute("close", true);
            }
        }
        catch(Exception e)
        {
            JSONObject item = new JSONObject();
            item.put("error", true);
            item.put("info", e.toString());
            out.print(item.toString());
        }
    }
    
    public Usuarios setData(HttpServletRequest req)
    {
        Usuarios u = new Usuarios();
        u.setNombre(req.getParameter("user"));
        u.setPass(req.getParameter("pass"));
        return u;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
