package com.connection;

import java.sql.*;

/**
 * Nombre de la clase: Conexion
 * Fecha: 12/10/2018
 * Version: 1.0
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class Conexion
{
    private Connection conn = null;

    public Conexion()
    {
        // Nada que hacer
    }
    
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
    public void conectar() throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = (Connection) DriverManager.getConnection
            (
                    "jdbc:mysql://localhost:3306/flotavehicular", "root", ""
            );
        } 
        catch (SQLException | ClassNotFoundException e) 
        {
            throw e;
        }
    }
    
    public void desconectar() throws Exception
    {
        try
        {
            if (conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            throw e;
        }
    }

}
