package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Juan Pablo Elias Hernández
 */
public class Chart extends Conexion
{
    public ArrayList getRuta() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT CONCAT(r.origen, ' a ', r.destino), COUNT(v.idRuta) AS cant " +
                    "FROM Viajes v INNER JOIN Rutas r ON " +
                    "v.idRuta = r.id WHERE v.estado = 1 " +
                    "GROUP BY v.idRuta ORDER BY cant DESC LIMIT 5;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'" + res.getString(1) + "'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getCant() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT COUNT(v.idRuta) AS cant " +
                    "FROM Viajes v INNER JOIN Rutas r ON " +
                    "v.idRuta = r.id WHERE v.estado = 1 " +
                    "GROUP BY v.idRuta ORDER BY cant DESC LIMIT 5;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add(res.getInt(1));
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getColors() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT stockAct, id FROM Producto ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'rgba(54, 162, 235, 0.2)'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getBorderColors() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT stockAct, id FROM Producto ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'rgba(54, 162, 235, 1)'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getMotorista() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT CONCAT(m.nombres), COUNT(v.idMotorista) AS cant " +
                    "FROM Viajes v INNER JOIN Motoristas m ON " +
                    "v.idMotorista = m.id WHERE v.estado = 1 " +
                    "GROUP BY v.idMotorista ORDER BY cant DESC LIMIT 5;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'" + res.getString(1) + "'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getCantMot() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT COUNT(v.idMotorista) AS cant " +
                    "FROM Viajes v INNER JOIN Motoristas m ON " +
                    "v.idMotorista = m.id WHERE v.estado = 1 " +
                    "GROUP BY v.idMotorista ORDER BY cant DESC LIMIT 5;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add(res.getInt(1));
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
}
