<%-- 
    Document   : index
    Created on : 12/10/2018, 10:31:26 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page import="com.models.Chart"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Dashboard del sistema">
    <meta name="author" content="Juan Pablo Elias Hernández">
    <title>Dashboard - Flota</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="img/icon.png" sizes="64x64">
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <a class="navbar-brand" href="index.jsp"><i class="fas fa-truck"></i> Flota Vehicular</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link text-center" href="index.jsp">
                        <i class="fa fa-fw fa-home"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Vehiculos">
                    <a class="nav-link text-center" href="vehiculos.jsp">
                        <i class="fa fa-fw fa-car"></i>
                        <span class="nav-link-text">Vehiculos</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Motoristas">
                    <a class="nav-link text-center" href="motoristas.jsp">
                        <i class="fa fa-fw fa-users"></i>
                        <span class="nav-link-text">Motoristas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Viajes">
                    <a class="nav-link text-center" href="viajes.jsp">
                        <i class="fa fa-fw fa-luggage-cart"></i>
                        <span class="nav-link-text">Viajes</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Rutas">
                    <a class="nav-link text-center" href="rutas.jsp">
                        <i class="fa fa-fw fa-map-signs"></i>
                        <span class="nav-link-text">Rutas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clientes">
                    <a class="nav-link text-center" href="clientes.jsp">
                        <i class="fa fa-fw fa-address-book"></i>
                        <span class="nav-link-text">Clientes</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Mantenimientos">
                    <a class="nav-link text-center" href="mantenimientos.jsp">
                        <i class="fa fa-fw fa-cogs"></i>
                        <span class="nav-link-text">Mantenimientos</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Talleres">
                    <a class="nav-link text-center" href="talleres.jsp">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">Talleres</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" align="center">
                    <button class="btn btn-light btn-sm" data-toggle="modal" data-target="#logoutModal">
                        <i class="fa fa-fw fa-sign-out-alt"></i> 
                        Cerrar sesión
                    </button>
                </li>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Header de la pagina -->
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <br>
                    <h1 align="center"> Bienvenido al sistema, <%= user %></h1>
                    <hr class="col-10"><br>
                </div>
            </div>
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.jsp">Dashboard</a>
                </li>
                <li class="breadcrumb-item active"></li>
            </ol>
            <div align="center" class="col-12">
                <br>
                <h2 class="text-dark">Graficas del sistema</h2>
                <hr class="col-10">
                <br>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 1rem;">
                    <div class="card">
                        <div class="card-header">
                            <h5><i class="fa fa-chart-bar"></i> Gráfica de 5 motoristas con más viajes</h5>
                        </div>
                        <div class="card-body">
                            <canvas id="pieChart"></canvas>
                        </div>
                        <div class="card-footer small text-muted">
                            Última actualización: <span id="time"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 1rem;">
                    <div class="card">
                        <div class="card-header">
                            <h5><i class="fa fa-chart-pie"></i> Gráfica de 5 rutas más frecuentes en viajes</h5>
                        </div>
                        <div class="card-body">
                            <canvas id="myChart"></canvas>
                        </div>
                        <div class="card-footer small text-muted">
                            Última actualización: <span id="time2"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div align="center" class="col-12">
                <br>
                <h2 class="text-dark">Modulos del sistema</h2>
                <hr class="col-10">
                <br>
            </div>
            <!-- Icon Cards-->
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-car"></i>
                            </div>
                            <div class="mr-5"><span id="vehCount"></span> Vehículos!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="vehiculos.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-users"></i>
                            </div>
                            <div class="mr-5"><span id="motCount"></span> Motoristas!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="motoristas.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-suitcase"></i>
                            </div>
                            <div class="mr-5"><span id="viaCount"></span> Viajes!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="viajes.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-map-signs"></i>
                            </div>
                            <div class="mr-5"><span id="rutCount"></span> Rutas!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="rutas.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-sm-6 mb-4">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-address-book"></i>
                            </div>
                            <div class="mr-5"><span id="cliCount"></span> Clientes!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="clientes.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-4">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-cogs"></i>
                            </div>
                            <div class="mr-5"><span id="manCount"></span> Mantenimientos!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="mantenimientos.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-12 mb-4">
                    <div class="card text-white bg-dark o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-wrench"></i>
                            </div>
                            <div class="mr-5"><span id="tallCount"></span> Talleres!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="talleres.jsp">
                            <span class="float-left">Ver Detalles</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div align="center" class="col-12">
                    <br>
                    <h2 class="text-dark">Reportes del sistema</h2>
                    <hr class="col-10">
                    <br>
                    <a href="Reports/reporteMantenimientos.jsp" class="btn btn-outline-dark">
                        <i class="fas fa-file-alt"></i> 
                        Reporte mantenimientos
                    </a>
                    <a href="Reports/reporteMotoristas.jsp" class="btn btn-outline-dark">
                        <i class="fas fa-file-alt"></i> 
                        Reporte motoristas
                    </a>
                    <a href="Reports/reporteVehiculos.jsp" class="btn btn-outline-dark">
                        <i class="fas fa-file-alt"></i> 
                        Reporte vehiculos
                    </a>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Flota Vehicular 2018</small>
                </div>
            </div>
        </footer>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <i class="fa fa-fw fa-sign-out-alt"></i>
                            Cerrar sesión
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Todos los cambios no guardados se perderan!
                        Si está seguro de cerrar la sesión actual, seleccione la opcion <b>Salir</b>.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                            <i class="fa fa-fw fa-undo"></i>
                            Cancelar
                        </button>
                        <button class="btn btn-outline-dark" id="logout">
                            <i class="fa fa-fw fa-sign-out-alt"></i>
                            Salir
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="js/sweetalert2.all.min.js" type="text/javascript"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin.js"></script>
        <script src="vendor/chart.js/Chart.js" type="text/javascript"></script>
        <!-- Custom scripts for this page-->
        <script src="vendor/font-awesome/js/all.js"></script>
        <script>
        <%
            if (request.getParameter("denied") != null)
            {
        %>
                swal({
                    title: 'Acceso denegado!',
                    html: 'Su usuario no tiene los <b>permisos</b> suficientes para realizar esta acción.<br>Contacte con un <b>administrador</b> del sistema para obtener un perfil con dichos permisos.',
                    type: 'error'
                });
        <%
            }
            Chart c = new Chart();
        %>
        </script>
        <script>
            (function ($)
            {
                charts();
                
                if ($(document).width() > 800)
                {
                    $("body").toggleClass("sidenav-toggled");
                    $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
                    $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
                }
                
                setInterval('charts()', 20000);
                
            })(jQuery);
            
            function charts ()
            {
                var ctx = $("#myChart");
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data:
                    {
                        labels: <%= c.getRuta()%>,
                        datasets: [{
                            label: 'Total ventas',
                            data: <%= c.getCant() %>,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.8)',
                                'rgba(54, 162, 235, 0.8)',
                                'rgba(255, 206, 86, 0.8)',
                                'rgba(75, 192, 192, 0.8)',
                                'rgba(153, 102, 255, 0.8)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)'
                            ],
                            borderWidth: 1
                        }]
                    }
                });
                
                var ctx2 = $("#pieChart");
                var myChart2 = new Chart(ctx2, {
                    type: 'bar',
                    data: {
                        labels: <%= c.getMotorista()%>,
                        datasets: [{
                            label: 'Stock actual',
                            data: <%= c.getCantMot()%>,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.8)',
                                'rgba(54, 162, 235, 0.8)',
                                'rgba(255, 206, 86, 0.8)',
                                'rgba(75, 192, 192, 0.8)',
                                'rgba(153, 102, 255, 0.8)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                  min: 0,
                                  maxTicksLimit: 5
                                },
                                gridLines: {
                                  display: true
                                },
                                beginAtZero:true
                            }]
                        },
                        legend: {
                          display: false
                        }
                    }
                });
                
                let cd = new Date();
                let dt = cd.getDate() + "/"
                    + (cd.getMonth()+1)  + "/" 
                    + cd.getFullYear() + " a las "  
                    + cd.getHours() + ":"  
                    + cd.getMinutes() + ":" 
                    + cd.getSeconds();   
                $('#time').html(dt); 
                $('#time2').html(dt);
            }
        </script>
    </div>
</body>
</html>
