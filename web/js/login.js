(function ()
{
    $('#btnLogin').click(function (e)
    {
        e.preventDefault();
        let form = $('#frm')[0];
        let user = $('#user').val();
        let pass = $('#pass').val();
        sendData(form, e, user, pass);
    });
})(jQuery);

// Funcion para enviar datos
function sendData(form, e, us, pass)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        let datos = {
            'action' : 'auth',
            'user' : us,
            'pass' : pass
        }
        $.post("controllerUsuarios", datos, function (res)
        {
            var data = JSON.parse(res);
            if (!data.error)
            {
                location.href = 'index.jsp';
            }
            else if (data.error)
            {
                $('#pass').val('');
                showError(data.info);
            }
        });
    }
    form.classList.add('was-validated');
}

function showError(error)
{
    $('.alert').html('');
    $('.alert').append(error);
    $('.alert').slideDown('slow');
    setTimeout(function(){
        $('.alert').slideUp('slow');
    }, 5000);
}