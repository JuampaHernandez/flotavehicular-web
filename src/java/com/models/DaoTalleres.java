package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Fecha: 13/10/2018
 * CopiRigth: Kevin Lovos
 * Nombre de la clase: DaoTalleres
 * Version: 1
 * @author Kevin Lovos
 */
public class DaoTalleres extends Conexion
{
   public List all() throws Exception
   {
        ResultSet res;
        List lstalleres = new ArrayList();
        try 
        {
            this.conectar();
            String sql="SELECT * FROM Talleres WHERE estado = 1 ORDER BY id DESC";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                Talleres ta = new Talleres();
                ta.setId(res.getInt(1));
                ta.setNombre(res.getString(2));
                ta.setDireccion(res.getString(3));
                ta.setTel(res.getString(4));
                ta.setFallas(res.getString(5));
                lstalleres.add(ta);
            }
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return lstalleres;
    }
   
    public void add( Talleres ta) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="INSERT INTO Talleres VALUES (NULL, ?, ?, ?, ?, 1)";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setString(1, ta.getNombre());
            pre.setString(2, ta.getDireccion());
            pre.setString(3, ta.getTel());
            pre.setString(4, ta.getFallas());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }

    public void edit(Talleres ta) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="UPDATE Talleres SET nombre = ?, direccion = ?, fallas = ?,"
                    + " telefono = ? WHERE id = ?";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setString(1,ta.getNombre());
            pre.setString(2, ta.getDireccion());
            pre.setString(3, ta.getFallas());
            pre.setString(4, ta.getTel());
            pre.setInt(5, ta.getId());
            pre.executeUpdate();

        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    public void delete(Talleres ta) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="UPDATE Talleres SET estado = 0 WHERE id = ?";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setInt(1, ta.getId());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
