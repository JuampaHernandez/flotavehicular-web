var table;
(function($) 
{   
    setInterval('all()', 20000);
    
    if ($(document).width() > 800)
    {
        $("body").toggleClass("sidenav-toggled");
        $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
        $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    }
    
    all();
    
    loadSelectV();
    
    loadSelectT();
    
    table = dt();
    
    $('.alphaonly').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z]/g,'') ); 
    });

    $("#new").on('click', function (e)
    {
        clean();
    });

    $("#refreshTable").on('click', function (e)
    {
        all();
    });
    
    $("#btnAdd").on('click', function (e)
    {
        var form = $('#frmAdd')[0];
        var idV = $('#idV').val();
        var idT = $('#idT').val();
        var tm = $('#tm').val();
        var fe = $('#fe').val();
        var fs = $('#fs').val();
        var faP = $('#faP').val();
        var faS = $('#faS').val();
        add(form, e, idV, idT, tm, fe, fs, faP, faS);
    });
    
    $("#btnEdit").on('click', function (e)
    {
        var form = $('#frmEdit')[0];
        var id = $('#idEdit').val();
        var idV = $('#idVEdit').val();
        var idT = $('#idTEdit').val();
        var tm = $('#tmEdit').val();
        var fe = $('#feEdit').val();
        var fs = $('#fsEdit').val();
        var faP = $('#faPEdit').val();
        var faS = $('#faSEdit').val();
        edit(form, e, id, idV, idT, tm, fe, fs, faP, faS);
    });
    
    $("#btnDel").on('click', function (e)
    {
        var id = $('#idDel').val();
        $('#deleteModal').modal('hide');
        del(id);
    });

})(jQuery);

function add (form, e, idV, idT, tm, fe, fs, faP, faS)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#addModal').modal('hide');
        let datos = {
            'action' : 'add',
            'id' : 0,
            'idV' : idV,
            'idT' : idT,
            'tm' : tm,
            'fe' : fe,
            'fs' : fs,
            'faP' : faP,
            'faS' : faS
        };
        $.post("controllerMantenimientos", datos, function (res)
        {
            var data = JSON.parse(res);
            if (!data.error)
            {
                swal({
                    title: 'Registro guardado!',
                    text: "La transaccón se ha realizado exitosamente.",
                    type: 'success'
                });
                all();
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al guardar!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    }
    form.classList.add('was-validated');
}

function edit (form, e, id, idV, idT, tm, fe, fs, faP, faS)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#editModal').modal('hide');
        swal({
            title: 'Seguro que desea editar el registro?',
            text: "Si selecciona 'Aceptar', la transaccion será realizada.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#138496',
            cancelButtonColor: '#5A6268',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value)
            {
                let datos = {
                    'action' : 'edit',
                    'id' : id,
                    'idV' : idV,
                    'idT' : idT,
                    'tm' : tm,
                    'fe' : fe,
                    'fs' : fs,
                    'faP' : faP,
                    'faS' : faS
                };
                $.post("controllerMantenimientos", datos, function (res)
                {
                    var data = JSON.parse(res);
                    if (!data.error)
                    {
                        swal({
                            title: 'Registro actualizado!',
                            text: "La transaccón se ha realizado exitosamente.",
                            type: 'success'
                        });
                        all();
                    }
                    else if (data.error)
                    {
                        swal({
                            title: 'Error al editar!',
                            text: "Algo salio mal en la transaccion: " + data.info,
                            type: 'error'
                        });
                    }
                });
            }
        });
    }
    form.classList.add('was-validated');
}

function del (id)
{
    swal({
        title: 'Seguro que desea eliminar el registro?',
        text: "Si selecciona 'Eliminar', la transaccion será realizada.",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#C82333',
        cancelButtonColor: '#5A6268',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            $.post("controllerMantenimientos", 
            { 
                'action' : 'delete',
                'id' : id 
            }, 
            function (res)
            {
                var data = JSON.parse(res);
                if (!data.error)
                {
                    swal({
                        title: 'Registro eliminado!',
                        text: "La transaccón se ha realizado exitosamente.",
                        type: 'success'
                    });
                    all();
                }
                else if (data.error)
                {
                    swal({
                        title: 'Error al eliminar!',
                        text: "Algo salio mal en la transaccion: " + data.info,
                        type: 'error'
                    });
                }
            });
        }
    });
}

function all ()
{
    $.post("controllerMantenimientos", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                $.each(JSON.parse(data.all), function(i, item) {
                    table.row.add([
                        item.vehiculo.placa, 
                        item.taller.nombre, 
                        item.tipoMantenimiento, 
                        item.fechaEntrada, 
                        item.fechaSalida,
                        item.fallaPrincipal,
                        item.fallaSecundaria,
                        `<button class="btn btn-info btn-sm" data-toggle="modal" 
                                data-target="#editModal" 
                                onclick="javascript:load(` + item.id + `,` + item.vehiculo.id + `, ` + item.taller.id + 
                                `, '` + item.tipoMantenimiento + `', '` + item.fechaEntrada + `', '` 
                                + item.fechaSalida + `', '` + item.fallaPrincipal + `', '` 
                                + item.fallaSecundaria + `')">
                            <i class="fa fa-edit"></i>
                        </button>   
                        <button class="btn btn-danger btn-sm" data-toggle="modal" 
                                data-target="#deleteModal" onclick="javascript:loadId(` + item.id + `)">
                            <i class="fas fa-fw fa-trash-alt"></i>
                        </button>`
                    ]).draw();
                });
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    let cd = new Date(); 
    let dt = "Ultima actualización " + cd.getDate() + "/"
            + (cd.getMonth()+1)  + "/" 
            + cd.getFullYear() + " a las "  
            + cd.getHours() + ":"  
            + cd.getMinutes() + ":" 
            + cd.getSeconds();
    $('#tableUpdateTime').html(dt);
//    console.log(dt);
}

function load(id, idV, idT, tm, fe, fs, faP, faS)
{
    $('#idEdit').val(id);
    $('#idVEdit').val(idV);
    $('#idTEdit').val(idT);
    $('#tmEdit').val(tm);
    $('#feEdit').val(fe);
    $('#fsEdit').val(fs);
    $('#faPEdit').val(faP);
    $('#faSEdit').val(faS);
}

function loadId(id)
{
    $('#idDel').val(id);
}

function clean()
{
    $('#idV').val('');
    $('#idT').val('');
    $('#tm').val('');
    $('#fe').val(moment().format('YYYY-MM-DD'));
    $('#fs').val(moment().format('YYYY-MM-DD'));
    $('#faP').val('');
    $('#faS').val('');
}

function dt()
{
    return $("#dataTable").DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}

function loadSelectV ()
{
    $.post("controllerVehiculos", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.placa + ` - ` + item.modelo + `
                        </option>`;
                });
                $('#idV').append(select);
                $('#idVEdit').append(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}

function loadSelectT ()
{
    $.post("controllerTalleres", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.nombre + `
                        </option>`;
                });
                $('#idT').html(select);
                $('#idTEdit').html(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}