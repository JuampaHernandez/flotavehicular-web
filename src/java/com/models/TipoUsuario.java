package com.models;

/**
 * Nombre de la clase: TipoUsuario
 * Fecha: 6/11/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Autor: Juan Pablo Elias Hernandez
 */
public class TipoUsuario
{
    private Integer id;
    private String descripcion;

    public TipoUsuario() {
    }

    public TipoUsuario(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
