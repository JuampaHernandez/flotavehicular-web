
package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.*;

/**
 * Nombre de la clase: DaoMantenimientos
 * Fecha: 3/10/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Autor: Juan Pablo Elias Hernandez
 */
public class DaoMantenimientos extends Conexion
{
    public List all() throws Exception
    {
        List ls = new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql = "{CALL showMant()}";
            PreparedStatement pst = this.getConn().prepareCall(sql);
            res = pst.executeQuery();
            while(res.next())
            {
                Mantenimientos m = new Mantenimientos();
                Vehiculo v = new Vehiculo();
                Talleres t = new Talleres();
                m.setId(res.getInt(1));
                v.setId(res.getInt(2));
                t.setId(res.getInt(3));
                m.setTipoMantenimiento(res.getString(4));
                m.setFechaEntrada(res.getString(5));
                m.setFechaSalida(res.getString(6));
                m.setFallaPrincipal(res.getString(7));
                m.setFallaSecundaria(res.getString(8));
                v.setPlaca(res.getString(10));
                t.setNombre(res.getString(11));
                m.setVehiculo(v);
                m.setTaller(t);
                ls.add(m);
            }
        }
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public void add(Mantenimientos m) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO Mantenimientos VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, 1);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, m.getVehiculo().getId());
            pst.setInt(2, m.getTaller().getId());
            pst.setString(3, m.getTipoMantenimiento());
            pst.setString(4, m.getFechaEntrada());
            pst.setString(5, m.getFechaSalida());
            pst.setString(6, m.getFallaPrincipal());
            pst.setString(7, m.getFallaSecundaria());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void edit(Mantenimientos m) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Mantenimientos SET idVehiculo = ?, idTaller = ?, " +
                "tipoMantenimiento = ?, fechaEntrada = ?, " +
                "fechaSalida = ?, fallaPrincipal = ?, " +
                "fallaSecundaria = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, m.getVehiculo().getId());
            pst.setInt(2, m.getTaller().getId());
            pst.setString(3, m.getTipoMantenimiento());
            pst.setString(4, m.getFechaEntrada());
            pst.setString(5, m.getFechaSalida());
            pst.setString(6, m.getFallaPrincipal());
            pst.setString(7, m.getFallaSecundaria());
            pst.setInt(8, m.getId());
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void delete(Mantenimientos m) throws Exception
    {
        try 
        {
            this.conectar();
            String sql ="UPDATE Mantenimientos SET estado = 0 WHERE id = ?";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, m.getId());
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
