
package com.models;

/**
 * Nombre de la clase: Clientes
 * Fecha: 12/10/2018
 * Version: 1.0
 * Copyright: Alvaro Perez
 * Autor: Alvaro Perez
 */
public class Clientes {
    private int id;
    private String nombres;
    private String apellidos;
    private int edad;
    private String genero;
    private String dui;
    private String direccion;
    private String telefono;
    private int estado;

    public Clientes() {
    }

    public Clientes(int id, String nombres, String apellidos, int edad, String genero, String dui, String direccion, String telefono, int estado) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.genero = genero;
        this.dui = dui;
        this.direccion = direccion;
        this.telefono = telefono;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    
}
