package com.models;

/**
 * Nombre de la clase: Viajes
 * Version: 1.0
 * Fecha: 6/11/2018
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernandez
 */
public class Viajes
{
    private int id;
    private Rutas ruta;
    private Motorista mot;
    private Vehiculo veh;
    private TipoCarga tipoCarga;
    private Clientes cli;
    private String fechaSalida;
    private String fechaEntrada;
    private Double cantidadCarga;

    public Viajes() {
    }

    public Viajes(int id, Rutas ruta, Motorista mot, Vehiculo veh, TipoCarga tipoCarga, Clientes cli, String fechaSalida, String fechaEntrada, Double cantidadCarga) {
        this.id = id;
        this.ruta = ruta;
        this.mot = mot;
        this.veh = veh;
        this.tipoCarga = tipoCarga;
        this.cli = cli;
        this.fechaSalida = fechaSalida;
        this.fechaEntrada = fechaEntrada;
        this.cantidadCarga = cantidadCarga;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Rutas getRuta() {
        return ruta;
    }

    public void setRuta(Rutas ruta) {
        this.ruta = ruta;
    }

    public Motorista getMot() {
        return mot;
    }

    public void setMot(Motorista mot) {
        this.mot = mot;
    }

    public Vehiculo getVeh() {
        return veh;
    }

    public void setVeh(Vehiculo veh) {
        this.veh = veh;
    }

    public TipoCarga getTipoCarga() {
        return tipoCarga;
    }

    public void setTipoCarga(TipoCarga tipoCarga) {
        this.tipoCarga = tipoCarga;
    }

    public Clientes getCli() {
        return cli;
    }

    public void setCli(Clientes cli) {
        this.cli = cli;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(String fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Double getCantidadCarga() {
        return cantidadCarga;
    }

    public void setCantidadCarga(Double cantidadCarga) {
        this.cantidadCarga = cantidadCarga;
    }
    
}
